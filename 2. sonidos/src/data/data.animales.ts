
export const ANIMALES = [
      {
        nombre: "Marcelo",
        imagen: "assets/animales/marcelo.png",
        audio: "assets/sonidos/caballo.mp3",
        duracion: 4,
        reproduciendo: false
      },
      {
        nombre: "Pablo",
        imagen: "assets/animales/pablo.png",
        audio: "assets/sonidos/cabra.wav",
        duracion: 4,
        reproduciendo: false
      },
      {
        nombre: "Patrico",
        imagen: "assets/animales/patricio.png",
        audio: "assets/sonidos/cerdo.wav",
        duracion: 2,
        reproduciendo: false
      },
      {
        nombre: "Jaime",
        imagen: "assets/animales/patricio.png",
        audio: "assets/sonidos/gallo.mp3",
        duracion: 4,
        reproduciendo: false
      },
      {
        nombre: "Matias",
        imagen: "assets/animales/patricio.png",
        audio: "assets/sonidos/mono.mp3",
        duracion: 8,
        reproduciendo: false
      },
      {
        nombre: "Karoline",
        imagen: "assets/animales/patricio.png",
        audio: "assets/sonidos/perro.mp3",
        duracion: 5,
        reproduciendo: false
      },
      {
        nombre: "Hugo",
        imagen: "assets/animales/patricio.png",
        audio: "assets/sonidos/serpiente.mp3",
        duracion: 2,
        reproduciendo: false
      },
      {
        nombre: "Roberto",
        imagen: "assets/animales/patricio.png",
        audio: "assets/sonidos/tigre.mp3",
        duracion: 2,
        reproduciendo: false
      }
];
